package com.twuc.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.backend.contract.CreateProductRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProductTest extends ApiTestBase {

    //productPost

    @Test
    void should_return_200_when_post_success() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐", 1L, "瓶", "url"))))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_400_when_value_is_null() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest(null, null, null, null))))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_create_product_when_request_correct() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐", 1L, "瓶", "url"))))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_400_when_product_name_is_repeat() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐", 1L, "瓶", "url"))))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void should_return_location_when_request_is_correct() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐", 1L, "瓶", "url"))))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", "/api/products/1"));
    }

    //productGet

    @Test
    void should_return_200_when_get_product() throws Exception {
        mockMvc.perform(get("/api/products"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_null_array_when_there_is_not_product() throws Exception {
        mockMvc.perform(get("/api/products"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.size()").value(0));
    }

    @Test
    void should_return_array_when_get_products() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐2", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐3", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/products"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.size()").value(3));
    }

    @Test
    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    void should_order_by_name_to_product_array() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐2", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐3", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐1", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/products"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.size()").value(3))
                .andExpect(jsonPath("$[0].name").value("可乐1"))
                .andExpect(jsonPath("$[1].name").value("可乐2"))
                .andExpect(jsonPath("$[2].name").value("可乐3"));
    }

}
