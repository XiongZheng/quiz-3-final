package com.twuc.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.backend.contract.CreateProductRequest;
import com.twuc.backend.domain.Order;
import com.twuc.backend.domain.OrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class OrderTest extends ApiTestBase {

    @Autowired
    private OrderRepository orderRepository;

    //Post
    @Test
    void should_return_400_when_product_not_exist() throws Exception {
        mockMvc.perform(post("/api/products/1/order"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_when_product_exit() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/products/1/order"))
                .andExpect(status().isOk());
    }

    @Test
    void should_save_order_when_product_exist() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/products/1/order"))
                .andExpect(status().isOk());

        Optional<Order> orderFound = orderRepository.findById(1L);

        assertTrue(orderFound.isPresent());
        assertEquals(Long.valueOf(1), orderFound.get().getProduct().getId());
    }

    //get
    @Test
    void should_return_null_array_when_orders_not_exist() throws Exception {
        mockMvc.perform(get("/api/orders"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.size()").value(0));
    }

    @Test
    void should_return_array_when_orders_are_exist() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐2", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐3", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new CreateProductRequest("可乐1", 1L, "瓶", "url"))))
                .andExpect(status().isOk());

        mockMvc.perform(post("/api/products/1/order"));
        mockMvc.perform(post("/api/products/2/order"));
        mockMvc.perform(post("/api/products/3/order"));

        mockMvc.perform(get("/api/orders"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.size()").value(3))
                .andExpect(jsonPath("$[0].name").value("可乐2"))
                .andExpect(jsonPath("$[1].name").value("可乐3"))
                .andExpect(jsonPath("$[2].name").value("可乐1"));
    }
}
