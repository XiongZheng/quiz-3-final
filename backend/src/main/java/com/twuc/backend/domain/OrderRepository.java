package com.twuc.backend.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    Order findByProductId(Long id);

    List<Order> findAllByOrderById();
}
