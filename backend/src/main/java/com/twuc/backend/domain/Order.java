package com.twuc.backend.domain;

import javax.persistence.*;

@Entity
@Table(name = "`order_details`")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer number;

    @OneToOne(optional = false)
    private Product product;

    public Order() {
    }

    public Order(Integer number) {
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public Integer getNumber() {
        return number;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void addNumber(){
        number++;
    }
}
