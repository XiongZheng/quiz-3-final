package com.twuc.backend.web;


import com.twuc.backend.contract.CreatProductResponse;
import com.twuc.backend.service.OrderService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class OrderController {

    private OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/products/{productId}/order")
    public ResponseEntity addOrder(@PathVariable Long productId){
        boolean order = orderService.createOrder(productId);
        if (order){
            return ResponseEntity
                    .ok()
                    .build();
        }

        return ResponseEntity
                .badRequest()
                .build();
    }

    @GetMapping("/orders")
    public ResponseEntity<CreatProductResponse[]> getOrders(){
        CreatProductResponse[] orders = orderService.getOrders();
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(orders);
    }
}
