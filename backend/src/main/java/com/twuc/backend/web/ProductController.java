package com.twuc.backend.web;

import com.twuc.backend.contract.CreateProductRequest;
import com.twuc.backend.domain.Product;
import com.twuc.backend.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @PostMapping("/products")
    public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest request) {
        Product savedProduct;

        try {
            savedProduct = productService.createProduct(request);
        } catch (IllegalArgumentException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .build();
        }
        return ResponseEntity.ok()
                .header("Location", "/api/products/" + savedProduct.getId())
                .header("Access-Control-Expose-Headers", "Location")
                .build();
    }

    @GetMapping("/products")
    public ResponseEntity<Product[]> getProducts(){
        Product[] products = productService.getProducts();

        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(products);
    }
}
