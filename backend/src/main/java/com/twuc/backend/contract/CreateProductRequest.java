package com.twuc.backend.contract;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateProductRequest {
    @NotNull
    @Size(min = 1)
    private String name;

    @NotNull
    @Min(0)
    private Long price;

    @NotNull
    @Size(min = 1)
    private String unit;

    @NotNull
    @Size(min = 1)
    private String url;

    public CreateProductRequest() {
    }

    public CreateProductRequest(@NotNull @Size(min = 1) String name, Long price, @NotNull @Size(min = 1) String unit, @NotNull @Size(min = 1) String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}
