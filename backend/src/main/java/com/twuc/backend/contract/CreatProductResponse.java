package com.twuc.backend.contract;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreatProductResponse {
    @NotNull
    @Size(min = 1)
    private String name;

    @NotNull
    @Min(0)
    private Long price;

    @NotNull
    @Size(min = 1)
    private String unit;

    @NotNull
    private Integer number;

    public CreatProductResponse() {
    }

    public CreatProductResponse(@NotNull @Size(min = 1) String name, @NotNull @Min(0) Long price, @NotNull @Size(min = 1) String unit, @NotNull Integer number) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public Integer getNumber() {
        return number;
    }
}
