package com.twuc.backend.service;

import com.twuc.backend.contract.CreateProductRequest;
import com.twuc.backend.domain.Product;
import com.twuc.backend.domain.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;


    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product createProduct(CreateProductRequest request){
        List<Product> productByName = productRepository.findAllByName(request.getName());

        if (productByName.size()>0){
            throw new IllegalArgumentException("商品名称已存在，请输入新的商品名称!");
        }

        Product product = new Product(request.getName(), request.getPrice(), request.getUnit(), request.getUrl());
        Product savedProduct = productRepository.saveAndFlush(product);

        return savedProduct;
    }

    public Product[] getProducts(){
        List<Product> products = productRepository.findAllByOrderByName();
        return products.toArray(Product[]::new);
    }
}
