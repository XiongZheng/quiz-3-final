package com.twuc.backend.service;

import com.twuc.backend.contract.CreatProductResponse;
import com.twuc.backend.domain.Order;
import com.twuc.backend.domain.OrderRepository;
import com.twuc.backend.domain.Product;
import com.twuc.backend.domain.ProductRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    private ProductRepository productRepository;

    private OrderRepository orderRepository;

    private EntityManager entityManager;

    public OrderService(ProductRepository productRepository, OrderRepository orderRepository, EntityManager entityManager) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.entityManager = entityManager;
    }

    public boolean createOrder(Long id) {

        Optional<Product> productFound = productRepository.findById(id);
        entityManager.clear();
        if (!productFound.isPresent()) {
            return false;
        }

        Order orders = orderRepository.findByProductId(id);
        if (orders == null) {
            Order order = new Order(1);
            productFound.get().setOrder(order);
            order.setProduct(productFound.get());
            orderRepository.saveAndFlush(order);
        } else {
            Order orderFound = orderRepository.findByProductId(id);
            orderFound.addNumber();
            orderRepository.flush();
            entityManager.clear();
        }
        return true;
    }

    public CreatProductResponse[] getOrders() {
        List<CreatProductResponse> orderResponses = new ArrayList<>();
        List<Order> orders = orderRepository.findAllByOrderById();

        if ((orders.size() == 0)) {
            return orderResponses.toArray(CreatProductResponse[]::new);
        }

        orders.forEach(order -> {
            Product product = order.getProduct();
            orderResponses.add(new CreatProductResponse(product.getName(), product.getPrice(), product.getUnit(), order.getNumber()));
        });
        return orderResponses.toArray(CreatProductResponse[]::new);
    }
}
