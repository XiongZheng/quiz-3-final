CREATE TABLE order_details (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    number INTEGER,
    product_id BIGINT,
    FOREIGN KEY(product_id) REFERENCES product(id)
);
