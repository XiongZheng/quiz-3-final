CREATE TABLE product (
        id BIGINT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(32) NOT NULL,
        price DOUBLE NOT NUll,
        unit VARCHAR(16) NOT NULL,
        url VARCHAR(255) NOT NULL
)
